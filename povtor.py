#/usr/bin/python3.8
# -*- coding: utf-8 -*-
import yaml
from colorama import Fore, Style
import os
import datetime

# Расположение переводов и повторение

perevody = "./повторение/"

def Povtor(file, word1, word2):
    povtor_dict = {}
    povtor_dict[word1] = word2
    today = datetime.datetime.today()
    with open(('{0}{1}_{2}.yaml').format(povtorenie, file, today.strftime("%Y-%m-%d-%H-%M")), 'a') as file_povtor:
        yaml.dump(povtor_dict, file_povtor, sort_keys=False, default_flow_style=False, allow_unicode=True)

def StyleBright(text):
    return Style.BRIGHT + text + Style.RESET_ALL
def Font(color, text):
    return color + text + Fore.RESET
def ZamenaSimvolov(text):
    text = text.replace("ё", "е").replace("-"," ").lower().strip()
    return text
def Perevod(file):
    with open(perevody + file + ".yaml", "r", encoding="utf-8") as words:
        my_words = yaml.safe_load(words)
        print(StyleBright("Вводите перевод слов и end вместо перевода, чтобы выйти из программы."))
        right_answer = 0
        for word in my_words:
            if isinstance(my_words[word], str) != True:
                my_words[word] = list(ZamenaSimvolov(i) for i in my_words[word])
            else:
                my_words[word] = ZamenaSimvolov(my_words[word])

            vvod = input("{0}: ".format(StyleBright(word))).lower()
            vvod = ZamenaSimvolov(vvod)

            if vvod in my_words[word] and not isinstance(my_words[word], str):
                print(Font(Fore.GREEN, "Верно"))
                right_answer += 1
            elif vvod == my_words[word]:
                print(Font(Fore.GREEN, "Верно"))
                right_answer += 1
            elif vvod.lower() == "end":
                ExitProgram()
            else:
                print(Font(Fore.RED, "Не верно! Правильный ответ: " + str(my_words[word])))
              #  Povtor(file, word, my_words[word])

        procent = int(((right_answer)/len(my_words))*100)
        result = StyleBright("Верных ответов {0}/{1} ({2}%)".format(right_answer, len(my_words), procent))
        print(result)
def PoiskYaml():
    list_yaml_files = []
    for yaml_file in os.listdir(perevody):
        if yaml_file.find("yaml") != -1:
            yaml_file = yaml_file.replace(".yaml", "")
            list_yaml_files.append(yaml_file)
    str_yaml_files = (", ").join(list_yaml_files)
    return str_yaml_files, list_yaml_files
def ExitProgram():
    print("")
    exit()

if __name__ == "__main__":
    while True:
        try:
            file = input("Выберите режим {0}: ".format(StyleBright(PoiskYaml()[0]))).lower().strip()
            if file.lower() in PoiskYaml()[1]:
                break
            elif file.lower() in "end":
                break
        except KeyboardInterrupt:
            ExitProgram()
        print("Такого режима не существует, повторите ввод или введите end для выхода.")

    if file.lower() != "end":
        try:
            Perevod(file)
        except KeyboardInterrupt:
            ExitProgram()